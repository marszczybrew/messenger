/// <reference path="../node_modules/@types/jquery.slimscroll/index.d.ts" />
var Ui;
(function (Ui) {
    class MessageView {
        constructor(msg) {
            this.msg = msg;
        }
        render() {
            let msg = this.msg;
            if (typeof msg == "undefined")
                return `<p>pls select a msg</p>`;
            return `
            <div class="panel panel-default">
                <div class="panel-heading">${msg.title} <button data-id="${msg.id}" class="btn btn-xs pull-right btn-primary btn-delete">
                    <i class="glyphicon glyphicon-trash"></i></button>
                </div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>From</dt>
                        <dd>${msg.from}</dd>
                        <dt>To</dt>
                        <dd>${msg.to}</dd>
                    </dl>
                    ${msg.content}
                </div>
            </div>
            `;
        }
    }
    class MessageListView {
        constructor(msgs) {
            this.msgs = msgs;
        }
        render() {
            let view = ``;
            for (let msg of this.msgs) {
                if (typeof msg != "undefined")
                    view += `
                    <li class="list-group-item">
                        <a class="message-link" data-id="${msg.id}">${msg.title}</a>
                    </li>`;
            }
            return view;
        }
    }
    class EditorView {
        constructor() {
            this.selector = document.querySelector('#editor');
            this.render();
        }
        show() {
            this.selector.classList.remove('hidden');
        }
        hide() {
            this.selector.classList.add('hidden');
        }
        render() {
            let view = `
                <form>
                    <label>To</label>
                    <input class="form-control" type="number" required name="to" id="editor-to"/>
                    <label>Whatchya ritin bout?</label>
                    <input type="text" id="editor-title" required class="form-control"/>
                    <label>Content</label>
                    <textarea class="form-control" required name="content" id="editor-content" rows="20" cols="40"></textarea>
                    <button class="btn btn-lg btn-danger" type="button" id="send-btn"><span class="glyphicon glyphicon-send"></span></button>
                </form>
            `;
            this.selector.innerHTML = view;
            document.querySelector("#send-btn").addEventListener('click', (e) => {
                e.preventDefault();
                return app.sendMessage();
            });
            return view;
        }
        getContent() {
            let msg = new Message(undefined, document.querySelector('#editor-title').value, document.querySelector('#editor-content').value, app.getId(), parseInt(document.querySelector('#editor-to').value));
            console.log(msg);
            return msg;
        }
    }
    Ui.EditorView = EditorView;
    class InboxView {
        constructor() {
            this.selector = document.querySelector('#inbox-view');
            this.listView = new MessageListView([]);
            this.contentView = new MessageView(undefined);
            this.render();
        }
        render() {
            let template = `
                <div class="col-md-5">
                    <ul class="list-group message-list" id="messages-list">
                        ${this.listView.render()}
                    </ul>
                </div>

                <div class="col-md-7">
                    <div id="message-content">${this.contentView.render()}</div>
                </div>            
            `;
            this.selector.innerHTML = template;
            let listSelector = document.querySelector('#messages-list');
            listSelector.addEventListener('click', (e) => {
                if (e.target && e.target.nodeName == 'A') {
                    if (typeof e.target.dataset.id != "undefined")
                        app.setActiveMessage(e.target.dataset.id);
                }
            });
            $(listSelector).slimScroll({
                height: '75vh'
            });
            if (document.querySelector('.btn-delete')) {
                document.querySelector('.btn-delete').addEventListener('click', (e) => {
                    let id;
                    if (e.target.nodeName == 'BUTTON')
                        id = e.target.dataset.id;
                    else
                        id = e.target.parentNode.dataset.id;
                    console.log(id);
                    app.deleteMessage(id);
                });
            }
            return template;
        }
        setMessages(data) {
            this.listView = new MessageListView(data);
            this.contentView = new MessageView(undefined);
            this.render();
        }
        setMessage(msg) {
            this.contentView = new MessageView(msg);
            this.render();
        }
        hide() {
            this.selector.classList.add('hidden');
        }
        show() {
            this.selector.classList.remove('hidden');
        }
    }
    Ui.InboxView = InboxView;
})(Ui || (Ui = {}));
//# sourceMappingURL=ui.js.map