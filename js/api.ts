/// <reference path="d.ts/index.d.ts" />
/// <reference path="../node_modules/retyped-bootbox-tsd-ambient/bootbox.d.ts" />
/// <reference path="ui.ts" />

import EditorView = Ui.EditorView;
import InboxView = Ui.InboxView;

class Api {
    private token: string = '';
    private id: number;
    private urlBase = 'http://edi.iem.pw.edu.pl/bach/mail/api';
    private username: string;
    private password: string;

    public constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
    }

    public getId() {
        // @todo fix this shit on api
        return this.id ? this.id : 1;
    }

    public isLoggedIn(): boolean {
        return this.token.length > 0;
    }

    private getUrl(endpoint: string) {
        return this.urlBase + endpoint;
    }

    public async login() {
        let that = this;
        let url = this.getUrl('/login');
        let request = new Request(url, {
            method: 'POST',
            body: JSON.stringify({
                login: that.username,
                password: that.password
            }),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        });
        return fetch(request).then(function (response) {
            return response.json().then(function (data) {
                if (typeof data.token == "undefined")
                    throw new Error("Invalid credentials");
                that.token = data.token;
                that.id = data.uid;
                console.log(data);
            });
        });
    }

    public async sendMessage(msg: Message) {
        let url = this.getUrl('/messages');
        let request = new Request(url, {
            method: 'post',
            headers: new Headers({
                token: this.token,
                'Content-type': 'application/json'
            }),
            body: JSON.stringify({
                content: msg.content,
                from: this.getId(),
                to: msg.to,
                subject: msg.title,
            })
        });

        return await fetch(request).then(async function (response) {
            await response.json().then(function (response) {
                console.log(response);
                return typeof response.id != undefined;
            })
        });
    }

    public async getUsers() {
        let url = this.getUrl('/users/');
        let request = new Request(url, {
            method: 'get',
            headers: new Headers({
                token: this.token
            })
        });
        let users = await fetch(request).then((data) => {
            return data.json()
        });
        return users;
    }

    public async deleteMessage(id: number) {
        let url = this.getUrl('/messages/' + id);
        let request = new Request(url, {
            method: 'delete',
            headers: new Headers({
                token: this.token
            })
        });
        return await fetch(request);
    }

    public async getAll() {
        let messages = [];
        let url = this.getUrl('/messages');
        let request = new Request(url, {
            method: 'get',
            headers: new Headers({
                'token': this.token
            })
        });
        await fetch(request).then(async function (response) {
            await response.json().then(function (row) {
                for (let raw of row) {
                    messages.push(new Message(raw.id, raw.subject, raw.content, raw.from, raw.to))
                }
                return messages;
            });
            return messages;
        });
        return messages;
    }
}

class Message {
    public constructor(public id: number, public title: string, public content: string, public from: number, public to: number) {
    }
}

class MessageRepository {
    private messages: Message[];
    private api: Api;

    public constructor(api: Api) {
        this.api = api;
        this.messages = [];
    }

    public async refresh() {
        await this.api.getAll().then((messages) => this.messages = messages);
        return this.messages;
    }

    public getAll() {
        return this.messages;
    }

    public getOne(id: number) {
        for (let msg of this.messages) {
            if (msg.id == id)
                return msg;
        }
        return undefined;
    }

    public  getFolder(f: Folder) {
        let data = [];
        for (let msg of this.messages) {
            if (f == Folder.Received && msg.to == this.api.getId()) {
                data.push(msg);
            } else if (f == Folder.Sent && msg.from == this.api.getId()) {
                data.push(msg);
            }
        }
        return data;
    }

    public async remove(id: number) {
        await this.api.deleteMessage(id);
        await this.refresh();
    }
}

enum Folder {
    Received, Sent
}

class App {
    private api: Api;
    private messages: MessageRepository;
    private folder: Folder = Folder.Received;
    private message: Message;
    private editor: EditorView;
    private inbox: InboxView;

    public constructor() {
        this.doLogin();
    }

    private async init() {
        this.inbox = new InboxView();
        this.editor = new EditorView();
        this.editor.hide();
        await this.api.login().catch((e) => {
            throw e;
        });
        this.messages = new MessageRepository(this.api);
        await this.messages.refresh();
        this.renderMessageList();
    }

    private renderMessageList() {
        this.inbox.setMessages(this.messages.getFolder(this.folder));
    }

    private renderMessage() {
        this.inbox.setMessage(this.message);
    }

    public setActiveFolder(folder: number) {
        this.folder = folder;
        this.editor.hide();
        this.inbox.show();
        this.renderMessageList();
    }

    public setActiveMessage(id: number) {
        let msg = this.messages.getOne(id);
        this.message = msg;
        this.renderMessage();
    }

    public showEditor() {
        this.inbox.hide();
        this.editor.show();
    }

    public async deleteMessage(id: number) {
        await this.messages.remove(id);
        this.renderMessageList();
    }

    private doLogin() {
        let that = this;
        bootbox.prompt("pls gimme ur login", (login) => {
            bootbox.prompt("now the pass", (password) => {
                that.api = new Api(login, password);
                that.init().catch((e) => {
                    console.error("could not authenticate");
                    console.error(e);
                    that.doLogin()
                });
            })
        })
    }

    sendMessage() {
        return this.api.sendMessage(this.editor.getContent());
    }

    getId() {
        return this.api.getId();
    }
}

let app = new App();
// folder switching
document.querySelector('#folder-list').addEventListener('click', (e) => {
    if (e.target && e.target.nodeName == 'A') {
        document.querySelector('.nav > .active').classList.remove('active');
        e.target.parentElement.classList.add('active');
        if (typeof e.target.dataset.folder != "undefined") {
            app.setActiveFolder(e.target.dataset.folder)
        }
    }
});
// editor tab logic
document.querySelector('#editor-link').addEventListener('click', (e) => {
    app.showEditor();
});