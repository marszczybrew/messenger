/// <reference path="d.ts/index.d.ts" />
/// <reference path="../node_modules/retyped-bootbox-tsd-ambient/bootbox.d.ts" />
/// <reference path="ui.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
var EditorView = Ui.EditorView;
var InboxView = Ui.InboxView;
class Api {
    constructor(username, password) {
        this.token = '';
        this.urlBase = 'http://edi.iem.pw.edu.pl/bach/mail/api';
        this.username = username;
        this.password = password;
    }
    getId() {
        // @todo fix this shit on api
        return this.id ? this.id : 1;
    }
    isLoggedIn() {
        return this.token.length > 0;
    }
    getUrl(endpoint) {
        return this.urlBase + endpoint;
    }
    login() {
        return __awaiter(this, void 0, void 0, function* () {
            let that = this;
            let url = this.getUrl('/login');
            let request = new Request(url, {
                method: 'POST',
                body: JSON.stringify({
                    login: that.username,
                    password: that.password
                }),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            });
            return fetch(request).then(function (response) {
                return response.json().then(function (data) {
                    if (typeof data.token == "undefined")
                        throw new Error("Invalid credentials");
                    that.token = data.token;
                    that.id = data.uid;
                    console.log(data);
                });
            });
        });
    }
    sendMessage(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = this.getUrl('/messages');
            let request = new Request(url, {
                method: 'post',
                headers: new Headers({
                    token: this.token,
                    'Content-type': 'application/json'
                }),
                body: JSON.stringify({
                    content: msg.content,
                    from: this.getId(),
                    to: msg.to,
                    subject: msg.title,
                })
            });
            return yield fetch(request).then(function (response) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield response.json().then(function (response) {
                        console.log(response);
                        return typeof response.id != undefined;
                    });
                });
            });
        });
    }
    getUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            let url = this.getUrl('/users/');
            let request = new Request(url, {
                method: 'get',
                headers: new Headers({
                    token: this.token
                })
            });
            let users = yield fetch(request).then((data) => {
                return data.json();
            });
            return users;
        });
    }
    deleteMessage(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = this.getUrl('/messages/' + id);
            let request = new Request(url, {
                method: 'delete',
                headers: new Headers({
                    token: this.token
                })
            });
            return yield fetch(request);
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            let messages = [];
            let url = this.getUrl('/messages');
            let request = new Request(url, {
                method: 'get',
                headers: new Headers({
                    'token': this.token
                })
            });
            yield fetch(request).then(function (response) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield response.json().then(function (row) {
                        for (let raw of row) {
                            messages.push(new Message(raw.id, raw.subject, raw.content, raw.from, raw.to));
                        }
                        return messages;
                    });
                    return messages;
                });
            });
            return messages;
        });
    }
}
class Message {
    constructor(id, title, content, from, to) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.from = from;
        this.to = to;
    }
}
class MessageRepository {
    constructor(api) {
        this.api = api;
        this.messages = [];
    }
    refresh() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.api.getAll().then((messages) => this.messages = messages);
            return this.messages;
        });
    }
    getAll() {
        return this.messages;
    }
    getOne(id) {
        for (let msg of this.messages) {
            if (msg.id == id)
                return msg;
        }
        return undefined;
    }
    getFolder(f) {
        let data = [];
        for (let msg of this.messages) {
            if (f == Folder.Received && msg.to == this.api.getId()) {
                data.push(msg);
            }
            else if (f == Folder.Sent && msg.from == this.api.getId()) {
                data.push(msg);
            }
        }
        return data;
    }
    remove(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.api.deleteMessage(id);
            yield this.refresh();
        });
    }
}
var Folder;
(function (Folder) {
    Folder[Folder["Received"] = 0] = "Received";
    Folder[Folder["Sent"] = 1] = "Sent";
})(Folder || (Folder = {}));
class App {
    constructor() {
        this.folder = Folder.Received;
        this.doLogin();
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.inbox = new InboxView();
            this.editor = new EditorView();
            this.editor.hide();
            yield this.api.login().catch((e) => {
                throw e;
            });
            this.messages = new MessageRepository(this.api);
            yield this.messages.refresh();
            this.renderMessageList();
        });
    }
    renderMessageList() {
        this.inbox.setMessages(this.messages.getFolder(this.folder));
    }
    renderMessage() {
        this.inbox.setMessage(this.message);
    }
    setActiveFolder(folder) {
        this.folder = folder;
        this.editor.hide();
        this.inbox.show();
        this.renderMessageList();
    }
    setActiveMessage(id) {
        let msg = this.messages.getOne(id);
        this.message = msg;
        this.renderMessage();
    }
    showEditor() {
        this.inbox.hide();
        this.editor.show();
    }
    deleteMessage(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.messages.remove(id);
            this.renderMessageList();
        });
    }
    doLogin() {
        let that = this;
        bootbox.prompt("pls gimme ur login", (login) => {
            bootbox.prompt("now the pass", (password) => {
                that.api = new Api(login, password);
                that.init().catch((e) => {
                    console.error("could not authenticate");
                    console.error(e);
                    that.doLogin();
                });
            });
        });
    }
    sendMessage() {
        return this.api.sendMessage(this.editor.getContent());
    }
    getId() {
        return this.api.getId();
    }
}
let app = new App();
// folder switching
document.querySelector('#folder-list').addEventListener('click', (e) => {
    if (e.target && e.target.nodeName == 'A') {
        document.querySelector('.nav > .active').classList.remove('active');
        e.target.parentElement.classList.add('active');
        if (typeof e.target.dataset.folder != "undefined") {
            app.setActiveFolder(e.target.dataset.folder);
        }
    }
});
// editor tab logic
document.querySelector('#editor-link').addEventListener('click', (e) => {
    app.showEditor();
});
//# sourceMappingURL=api.js.map